octave-matgeom (1.2.4-2) unstable; urgency=medium

  * d/control: Bump Standards-Version to 4.7.0 (no changes needed)
  * d/p/drop-polynomialCurves2d.patch: New patch (Closes: #1083243)

 -- Rafael Laboissière <rafael@debian.org>  Sat, 12 Oct 2024 12:42:30 -0300

octave-matgeom (1.2.4-1) unstable; urgency=medium

  * New upstream version 1.2.4
  * d/copyright: Reflect upstream changes
  * d/control:
    + Bump Standards-Version to 4.6.2 (no changes needed)
    + Update Homepage URL
  * d/u/metadata: Update Bug-Database and Bug-Submit URLs
  * d/rules
    + Adjust renaming of upstream documentation files
    + Drop the removal of file sedgewick_points.txt

 -- Rafael Laboissière <rafael@debian.org>  Sun, 24 Mar 2024 15:18:39 -0300

octave-matgeom (1.2.3-3) unstable; urgency=medium

  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Contact.
  * Remove obsolete field Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).
  * d/control: Bump Standards-Version to 4.6.1 (no changes needed)
  * Build-depend on dh-sequence-octave
    + d/control: Ditto
    + d/rules: Drop the --with=octave option from dh call
  * d/watch: Adjust for new URL at gnu-octave.github.io
  * d/s/lintian-override: Override Lintian warning
    debian-watch-lacks-sourceforge-redirector
  * d/copyright: Update Copyright years for debian/* files

 -- Rafael Laboissière <rafael@debian.org>  Mon, 05 Dec 2022 08:02:49 -0300

octave-matgeom (1.2.3-2) unstable; urgency=medium

  * Upload to unstable

 -- Rafael Laboissière <rafael@debian.org>  Mon, 16 Aug 2021 12:37:14 -0300

octave-matgeom (1.2.3-1) experimental; urgency=medium

  * New upstream version 1.2.3
  * d/copyright: Reflect upstream changes
  * d/control: Bump Standards-Version to 4.5.1 (no changes needed)
  * d/rules: Remove useless file sedgewick_points.txt

 -- Rafael Laboissière <rafael@debian.org>  Mon, 07 Jun 2021 08:53:20 -0300

octave-matgeom (1.2.2-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Archive, Name, Repository.

  [ Rafael Laboissière ]
  * d/u/metadata: Use  URL of the sf.net Git repository
  * d/rules:
    + Use execute_after_ instead of override_dh_installdeb
    + Install upstream changelogs into /usr/shar/doc

 -- Rafael Laboissière <rafael@debian.org>  Sat, 14 Nov 2020 05:36:34 -0300

octave-matgeom (1.2.2-2) unstable; urgency=medium

  * d/u/metadata: New file
  * d/control: Bump debhelper compatibitlity level to 13

 -- Rafael Laboissière <rafael@debian.org>  Tue, 26 May 2020 06:41:52 -0300

octave-matgeom (1.2.2-1) unstable; urgency=low

  * Initial release (closes: #950996)

 -- Rafael Laboissière <rafael@debian.org>  Sun, 09 Feb 2020 06:48:20 -0300
